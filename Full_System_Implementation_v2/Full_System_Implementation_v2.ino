#include <avr/io.h>
#include <avr/interrupt.h>

/* Pin Layout
    13 LED
    12 NONE
    11 Distance // On Main Project Only
    10 SPWM
    09 SPWM
    08 ENABLE
    07 ENABLE
    06 Flame Sensor 1
    05 Flame Sensor 2 // On Main Project Only
    04 Flame Sensor 3 // On Main Project Only
    03 Flame Sensor 4 // On Main Project Only
    02
    01
    00 ...?
*/

#define FLAME_DETECTOR0 6
//#define FLAME_DETECTOR1 5
//#define FLAME_DETECTOR2 4
//#define FLAME_DETECTOR3 3

#define DELAY 7000


static int flame = HIGH;
//static int flame1 = HIGH;
//static int flame2 = HIGH;
//static int flame3 = HIGH;
//static int flame4 = HIGH;
static int flameo = 0;
//static int pingPin = 11;
//static long duration, cm;


// Look up tables with 200 entries each, normalised to have max value of 1600 which is the period of the PWM loaded into register ICR1.
int lookUp1[] = {50 , 100 , 151 , 201 , 250 , 300 , 349 , 398 , 446 , 494 , 542 , 589 , 635 , 681 , 726 , 771 , 814 , 857 , 899 , 940 , 981 , 1020 , 1058 , 1095 , 1131 , 1166 , 1200 , 1233 , 1264 , 1294 , 1323 , 1351 , 1377 , 1402 , 1426 , 1448 , 1468 , 1488 , 1505 , 1522 , 1536 , 1550 , 1561 , 1572 , 1580 , 1587 , 1593 , 1597 , 1599 , 1600 , 1599 , 1597 , 1593 , 1587 , 1580 , 1572 , 1561 , 1550 , 1536 , 1522 , 1505 , 1488 , 1468 , 1448 , 1426 , 1402 , 1377 , 1351 , 1323 , 1294 , 1264 , 1233 , 1200 , 1166 , 1131 , 1095 , 1058 , 1020 , 981 , 940 , 899 , 857 , 814 , 771 , 726 , 681 , 635 , 589 , 542 , 494 , 446 , 398 , 349 , 300 , 250 , 201 , 151 , 100 , 50 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0};
int lookUp2[] = {0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 50 , 100 , 151 , 201 , 250 , 300 , 349 , 398 , 446 , 494 , 542 , 589 , 635 , 681 , 726 , 771 , 814 , 857 , 899 , 940 , 981 , 1020 , 1058 , 1095 , 1131 , 1166 , 1200 , 1233 , 1264 , 1294 , 1323 , 1351 , 1377 , 1402 , 1426 , 1448 , 1468 , 1488 , 1505 , 1522 , 1536 , 1550 , 1561 , 1572 , 1580 , 1587 , 1593 , 1597 , 1599 , 1600 , 1599 , 1597 , 1593 , 1587 , 1580 , 1572 , 1561 , 1550 , 1536 , 1522 , 1505 , 1488 , 1468 , 1448 , 1426 , 1402 , 1377 , 1351 , 1323 , 1294 , 1264 , 1233 , 1200 , 1166 , 1131 , 1095 , 1058 , 1020 , 981 , 940 , 899 , 857 , 814 , 771 , 726 , 681 , 635 , 589 , 542 , 494 , 446 , 398 , 349 , 300 , 250 , 201 , 151 , 100 , 50 , 0};

static int num;
static unsigned int delay_num = 0;
static bool delay_flag = false;
enum sys_state {
  INIT,
  NO_SOUND,
  SOUND,
  DELAY_STOP,
};

enum sys_state soundwave = INIT;

void setup() {
  //Serial.begin(115200);
  // Register initilisation, see datasheet for more detail.
  TCCR1A = 0b10100010;
  /*10 clear on match, set at BOTTOM for compA.
    10 clear on match, set at BOTTOM for compB.
    00
    10 WGM1 1:0 for waveform 15.
  */
  TCCR1B = 0b00011001;
  /*000
    11 WGM1 3:2 for waveform 15.
    001 no prescale on the counter.
  */
  TIMSK1 = 0b00000001;
  /*0000000
    1 TOV1 Flag interrupt enable.
  */
  //pin 9 and 10 are the outputs
  ICR1   = 1600;     // Period for 16MHz crystal, for a switching frequency of 100KHz for 200 subdevisions per 50Hz sin wave cycle.
  sei();             // Enable global interrupts.
  DDRB = 0b00000110; // Set PB1 and PB2 as outputs.
  pinMode(13, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(FLAME_DETECTOR0, INPUT);
  //pinMode(FLAME_DETECTOR1, INPUT);
  //pinMode(FLAME_DETECTOR2, INPUT);
  //pinMode(FLAME_DETECTOR3, INPUT);

}

void loop() {
  //  flame = digitalRead(FLAME_DETECTOR0); //Digital Read for Flame sensor
  //  flame1 = digitalRead(FLAME_DETECTOR1); //Digital Read for Flame sensor
  //  flame2 = digitalRead(FLAME_DETECTOR2); //Digital Read for Flame sensor
  //  flame3 = digitalRead(FLAME_DETECTOR3); //Digital Read for Flame sensor

  //pinMode(pingPin, OUTPUT);
  //digitalWrite(pingPin, LOW);
  //delayMicroseconds(2);
  //digitalWrite(pingPin, HIGH);
  //delayMicroseconds(5);
  //digitalWrite(pingPin, LOW);
  //pinMode(pingPin, INPUT);
  //duration = pulseIn(pingPin, HIGH);
  //cm = microsecondsToCentimeters(duration);


  switch (soundwave) {
    case INIT:
    case NO_SOUND:
      digitalWrite(7, LOW);
      digitalWrite(8, LOW);
      OCR1A = 0;
      OCR1B = 0;
      break;
    default:
      digitalWrite(7, HIGH);
      digitalWrite(8, HIGH);
      break;
  }

  //  if (flame == HIGH) { // When flame isn't detected
  //    digitalWrite(7, LOW);
  //    digitalWrite(8, LOW);
  //    //delay(1000)

}

ISR(TIMER1_OVF_vect) { // ISR for Timer
  //static int num;
  static char trig;
  // change duty-cycle every period.
  flame = digitalRead(FLAME_DETECTOR0); //Digital Read for Flame sensor
  //flame1 = digitalRead(FLAME_DETECTOR1); //Digital Read for Flame sensor
  //flame2 = digitalRead(FLAME_DETECTOR2); //Digital Read for Flame sensor
  //flame3 = digitalRead(FLAME_DETECTOR3); //Digital Read for Flame sensor
  flameo = flame;// & flame1 & flame2 & flame3;
  switch (soundwave) {
    case INIT:
      soundwave = NO_SOUND;
      break;

    case NO_SOUND:
      //Serial.println("State: NO_SOUND\n");
      if (flameo == HIGH){// || cm > 60) {
        OCR1A = 0;
        OCR1B = 0;
        //Serial.println ("NOT DETECTED\n");
      }
      else {
        soundwave = SOUND;
      }
      break;

    case SOUND:
      //Serial.println("State: SOUND\n");
      if (flameo == LOW){// || cm < 60) {
        OCR1A = lookUp1[num]; // spwm array
        OCR1B = lookUp2[num]; // same spwm array but 180 shift
        if (++num >= 200) { // Pre-increment num then check it's below 200. ONLY FOR LED
          num = 0;       // Reset num.
          trig = trig ^ 0b00000001;
          digitalWrite(13, trig);
          //delay(1000);
        }
      }
      else {
        soundwave = DELAY_STOP;
      }
      break;

    case DELAY_STOP:
      //Serial.println("State: DELAY_STOP\n");
      OCR1A = lookUp1[num]; // spwm array
      OCR1B = lookUp2[num]; // same spwm array but 180 shift
      if (++num >= 200) { // Pre-increment num then check it's below 200. ONLY FOR LED
        num = 0;       // Reset num.
        trig = trig ^ 0b00000001;
        //digitalWrite(13, trig);
        //delay(1000);
      }

      else if (flameo == LOW){// || cm < 60) {
        soundwave = SOUND;
        delay_num = 0; //Reset Delay
      }
      else {
        delay_num++;
      }
      //delay_num++;

      if (delay_num == DELAY) { // When delay is done, transition back to NO_SOUND
        soundwave = NO_SOUND;
        delay_num = 0;
      }

      break;

  }

  //  if (flame == HIGH) { // If it's high, print
  //    Serial.println ("NOT DETECTED");
  //    delay_flag = true;
  //    //delay(1000);
  //  }
  //  else { //Or's trigger
  //    OCR1A = lookUp1[num]; // spwm array
  //    OCR1B = lookUp2[num]; // same spwm array but 180 shift
  //    if (++num >= 200) { // Pre-increment num then check it's below 200. ONLY FOR LED
  //      num = 0;       // Reset num.
  //      trig = trig ^ 0b00000001;
  //      digitalWrite(13, trig);
  //      //delay(1000);
  //    }
  //  }
  //
  //  if (delay_flag == true){
  //    for(int i = 0; i = 1000; i++){
  //      OCR1A = lookUp1[num]; // spwm array
  //      OCR1B = lookUp2[num]; // same spwm array but 180 shift
  //    }
  //    delay_flag == false;
  //  }

}

/*long microsecondsToCentimeters(long microseconds) {
  // The speed of sound is 340 m/s or 29 microseconds per centimeter.
  // The ping travels out and back, so to find the distance of the object we
  // take half of the distance travelled.
  return microseconds / 29 / 2;
}*/
