//
//    FILE: functionGeneratorDuoPlot.ino
//  AUTHOR: Rob Tillaart
// PURPOSE: demo function generators
//    DATE: 2020-06-10
//     URL: https://github.com/RobTillaart/FunctionGenerator
//
// use a Serial plotter to show the data


#include "functionGenerator.h"


#define TRIG 8
#define SIN 9
#define BIAS 127.5
funcgen gen1;
funcgen gen2;


void setup()
{
  Serial.begin(115200);
  // Serial.print("Start functionGeneratorPerformance - LIB VERSION: ");
  // Serial.println(FUNCTIONGENERATOR_LIB_VERSION);
  pinMode(TRIG,OUTPUT);
  pinMode(SIN,OUTPUT);
  
  /*gen1.setFrequency(10);
  gen1.setAmplitude(50);
  gen1.setYShift(BIAS);


  gen2.setFrequency(17);
  gen2.setAmplitude(50);
  gen2.setYShift(BIAS);*/
}


void loop()
{
  digitalWrite(TRIG,HIGH);
  delay(50);
  digitalWrite(TRIG,LOW);
  delay(50);
  /*float t = millis() * 0.001;
  
  float sinValue = gen1.sinus(t);
  analogWrite(SIN,sinValue);
  float trigValue = gen2.square(t);
  analogWrite(TRIG,trigValue);*/

}
