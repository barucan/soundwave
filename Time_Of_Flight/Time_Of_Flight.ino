//#######################################
// Brain-controller for ZS-X11h bldc-board from China shops
// now testing on Arduino UNO  
//#######################################
int trottlePin = A1;   // analog pin 2
int breakPin = A2;   // analog pin 3
int trottleOut = 6;
int breakOut = 4;
//#######################################
//#######################################

//#######################################
//#######################################


void setup() {
  // pinMode(trottlePin, INPUT);
  // pinMode(breakPin, INPUT);
  // pinMode(analogOutput, OUTPUT);
 pinMode(8, OUTPUT);
 pinMode(9, OUTPUT);
 digitalWrite(8, HIGH);
 digitalWrite(9, HIGH);
  // put your setup code here, to run once:
     Serial.begin(115200);
  // Serial.begin(9600);
}

void loop() {

  //  digitalWrite(trottlePin, HIGH);
  //  digitalWrite(breakPin, HIGH);
  int trottleVal = analogRead(trottlePin);  // read the input pin
  delay(40);
  int breakVal = analogRead(breakPin);


  int outSignal = 0;
  if (breakVal <= 440) {
    outSignal = analogValue(trottleVal) - analogValue(breakVal);
    analogWrite(breakOut, 0);
    }
  else {
    outSignal = 0;
    analogWrite(breakOut, 255);
    };
  outSignal = constrain(outSignal, 0, 255);

  Serial.print(" *trottle: ");
  Serial.print(trottleVal);
  Serial.print(" *break: ");
  Serial.print(breakVal);
  Serial.print(" *output: ");
  Serial.println(outSignal);
  analogWrite(trottleOut, outSignal);

}

int analogValue(int hallVal) {
  //  Serial.println(hallVal);
  int x1 = 185;
  int x2 = 800;
  int y1 = 0;
  int y2 = 255;
  int y = y1 + (hallVal - x1) * ((float)(y2 - y1) / (x2 - x1));
  y = constrain(y, y1, y2);
  //  delay(50);
  //Serial.print(y);
  //Serial.print(" - ");
  return y;

  //  analogWrite(analogOutput, y);

}
