#define FLAME_DETECTOR 7
int flameo = HIGH;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  pinMode(FLAME_DETECTOR, INPUT);

}

void loop() {
  // put your main code here, to run repeatedly:
  flameo = digitalRead(FLAME_DETECTOR);
  if(flameo == HIGH){
    Serial.println ("FLAME NOT DETECTED");
    
  }else{
    Serial.println ("FLAME DETECTED");
  }

}
